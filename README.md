# Direct Coupling Analysis

Implementation of mean-field DCA in tensorflow2.

Other software for DCA, and interpretable Neural Network Classifiers can be found at *https://gitlab.com/LBS-EPFL/code/lbsNN/-/tree/v3.0*